'use strict';

import React, { Component } from 'react';
import { Router, Scene, Stack } from 'react-native-router-flux';
import StartScreen from './components/StartScreen';
import Home from './components/Home';
import LocalMap from './components/LocalMap';
import Directions from './components/Directions';
import DirectionsParking from './components/DirectionsParking';

// Main Router stack, add the navigation screens here. StartScreen is the initial screen to land.
class Routers extends Component { 
  render() {
    return (
    <Router>
		<Stack key="root">
      <Scene
        key="start"
        component={ StartScreen }
        initial= {true}
        hideNavBar={true} 
      />
      <Scene
        key="map" 
        component={ LocalMap }
        title="FINDIT" 
        hideNavBar={true} 
        navigationBarStyle={{backgroundColor: '#5361bc', borderRadius: 0}} 
        leftButtonIconStyle = {{ tintColor:'#5361bc'}}
      />
      <Scene
        key="home" 
        component={ Home }
        title="FINDIT" 
        hideNavBar={true} 
        navigationBarStyle={{backgroundColor: '#5361bc', borderRadius: 0}} 
        leftButtonIconStyle = {{ tintColor:'#5361bc'}}
      />
      <Scene
        key="directions" 
        component={ Directions }
        title="Directions" 
        hideNavBar={true} 
        navigationBarStyle={{backgroundColor: '#5361bc', borderRadius: 0}} 
        leftButtonIconStyle = {{ tintColor:'#5361bc'}}
      />
      <Scene
        key="directionsParking" 
        component={ DirectionsParking }
        title="Directions Parking" 
        hideNavBar={true} 
        navigationBarStyle={{backgroundColor: '#5361bc', borderRadius: 0}} 
        leftButtonIconStyle = {{ tintColor:'#5361bc'}}
      />
		</Stack>
  	</Router> 
    );
  }
}

export default Routers;