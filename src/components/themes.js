import MapboxGL from '@mapbox/react-native-mapbox-gl';
import StoreLocatorKit from '@mapbox/store-locator-react-native';

import parkingUnselectedIcon from '../assets/images/Icons/parking_unpinned_64.png';
import parkingSelectedIcon from '../assets/images/Icons/Parking-Pinned.png';
import parkingIcon from '../assets/images/Icons/parking_unpinned_64.png';

import toiletUnselectedIcon from '../assets/images/Icons/toilet_unpinned_64.png';
import toiletSelectedIcon from '../assets/images/Icons/toilet-pinned.png';
import toiletIcon from '../assets/images/Icons/toilet_unpinned_64.png';

export const parkingTheme = new StoreLocatorKit.Theme({
  icon: parkingUnselectedIcon ,
  activeIcon: parkingSelectedIcon,
  styleURL: 'mapbox://styles/uvishere/cjgz9ao04000f2snu34b9j4jj',
  primaryColor: '#45AAE9',
  primaryDarkColor: '#268DBA',
  directionsLineColor: '#6ECAF1',
  cardIcon: parkingIcon,
  cardTextColor: '#1082B2',
  accentColor: '#9FCCE0',
});

export const toiletTheme = new StoreLocatorKit.Theme({
  icon: toiletUnselectedIcon ,
  activeIcon: toiletSelectedIcon,
  styleURL: 'mapbox://styles/uvishere/cjgz9ao04000f2snu34b9j4jj',
  primaryColor: '#45AAE9',
  primaryDarkColor: '#268DBA',
  directionsLineColor: '#6ECAF1',
  cardIcon: toiletIcon,
  cardTextColor: '#b2a110',
  accentColor: '#e0c19f',
});
