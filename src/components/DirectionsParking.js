import React from 'react';

import {
  View,
  Text,
  Platform,
  Modal,
  FlatList,
  Image,
  TouchableOpacity,
  StatusBar,
  StyleSheet,
  ToastAndroid
} from 'react-native';

import MapboxGL from '@mapbox/react-native-mapbox-gl';
import StoreLocatorKit from '@mapbox/store-locator-react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import LocationServicesDialogBox from "react-native-android-location-services-dialog-box";


import parkingsData from '../assets/parkings.json';
import toiletsData from '../assets/toilets.json';

import {
  parkingTheme
} from './themes';

const IS_IOS = Platform.OS === 'ios';
const MAPBOX_ACCESS_TOKEN = 'pk.eyJ1IjoidXZpc2hlcmUiLCJhIjoiY2pleHBjOWtjMTZidTJ3bWoza3dlZmIxZiJ9.HvLEBmq44mUfdgT7-C73Jg';

const ThemeList = [
  {
    name: 'Parking Theme',
    theme: parkingTheme,
    image: require('../assets/images/pin.png'),
  },
];


class DirectionsParking extends React.Component {
  constructor (props) {
    super(props);
    console.log(this.props.theme); 
    this.state = {
      isGranted: IS_IOS,
      activeTheme: ThemeList[0].theme,
      initialLocation: [130.871, -12.373],
    };

    let theme = null;
    this.onDismiss = this.onDismiss.bind(this);
  }

  async componentWillMount () {
    if (!IS_IOS) {
      const isGranted = await MapboxGL.requestAndroidLocationPermissions();

    
      this.getLocationFromDevice();
      this.setState({ isGranted: isGranted });
    }
    MapboxGL.setAccessToken(MAPBOX_ACCESS_TOKEN);
  }

  //get location from device and update the state
  getLocationFromDevice() {
    
    return (
      LocationServicesDialogBox.checkLocationServicesIsEnabled({
          message: "<h2>Use Location ?</h2>This app wants to change your device settings:<br/><br/>Use GPS, Wi-Fi, and cell network for location<br/><br/>",
          ok: "YES",
          cancel: "No"
        })
        .then((success) => {
          navigator.geolocation.getCurrentPosition((position) => {
            const {latitude, longitude} = position.coords
    
            console.log(latitude + ", " + longitude)

            this.setState({
              initialLocation: [longitude,latitude]
              })
    
          }, (error) => ToastAndroid.show(error)) , { enableHighAccuracy: false, timeout: 20000, maximumAge: 1000 }
        })
        .catch((error) => {
          ToastAndroid.show(" :( \ nLocation Not Enabled. Please Enable the location service and try again!!")
      })
    )
  }
  onDismiss () {
    StatusBar.setBarStyle('dark-content');
    this.setState({ activeTheme: null });
  }

  renderMap () {
    if (!this.state.activeTheme) {
      return null;
    }

    StatusBar.setBarStyle('light-content');

    return (
      <Modal
        visible={!!this.state.activeTheme}
        animationType='slide'
        transparent
        onRequestClose={this.onDismiss}>
        <View style={styles.matchParent}>
          <StoreLocatorKit.MapView
            accessToken={MAPBOX_ACCESS_TOKEN}
            theme={this.state.activeTheme}
            centerCoordinate={this.state.initialLocation}
            featureCollection={parkingsData}
            directionType="mapbox/walking"
            style={styles.matchParent} />

          <View style={styles.mapHeader}>
            <Icon
              name='keyboard-backspace'
              size={28}
              onPress={this.onDismiss}
              style={styles.backArrow}
              color='white' />

            <Text style={styles.mapHeaderText}>Available Amenities</Text>
          </View>
        </View>
      </Modal>
    );
  }


  render () {
    if (!this.state.isGranted) {
      return null;
    }
    return (
      <View style={styles.matchParent}>
        {this.renderMap()}
      </View>
    );
  }
}
const styles = StyleSheet.create({
  matchParent: {
    flex: 1,
  },
  mapHeader: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    height: 120,
    backgroundColor: 'transparent',
  },
  mapGradient: {
    flex: 1,
    height: 120,
  },
  mapHeaderText: {
    fontSize: 24,
    color: 'white',
    alignSelf: 'center',
    position: 'relative',
    top: -60,
  },
  backArrow: {
    position: 'absolute',
    top: 32,
    left: 24,
  },
});

export default DirectionsParking;