import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';
import MapboxGL from '@mapbox/react-native-mapbox-gl';
import LocationServicesDialogBox from "react-native-android-location-services-dialog-box";

MapboxGL.setAccessToken('pk.eyJ1IjoidXZpc2hlcmUiLCJhIjoiY2pleHBjOWtjMTZidTJ3bWoza3dlZmIxZiJ9.HvLEBmq44mUfdgT7-C73Jg');

export default class LocalMap extends Component {
    constructor(props) {
    super(props);

    this.state = {
      isAndroidPermissionGranted: false,
      locationCords: []
    };
  };

  //render User Location
  renderAnnotations () {
    const {latitude, longitude} = this.state.locationCords
      console.log(latitude + ", " + longitude)

    coords=[latitude, longitude];
    console.log(coords);
    return (
      <MapboxGL.PointAnnotation
        key='pointAnnotation'
        id='pointAnnotation'
        coordinate={[130.3423,-12.3432]}>

        <View style={styles.annotationContainer}>
          <View style={styles.annotationFill} />
        </View>
        <MapboxGL.Callout title='You are Here!' />
      </MapboxGL.PointAnnotation>
    )
  }

  //get location from device and update the state
  getLocationFromDevice() {
    return (
      LocationServicesDialogBox.checkLocationServicesIsEnabled({
          message: "<h2>Use Location ?</h2>This app wants to change your device settings:<br/><br/>Use GPS, Wi-Fi, and cell network for location<br/><br/>",
          ok: "YES",
          cancel: "No"
        })
        .then((success) => {
          navigator.geolocation.getCurrentPosition((position) => {
            const {latitude, longitude} = position.coords
    
            console.log(latitude + ", " + longitude)

            this.setState({
              locationCords: position.coords
              })
    
          }, (error) => this.showMsg(error)) , { enableHighAccuracy: false, timeout: 20000, maximumAge: 1000 }
        })
        .catch((error) => {
          this.showMsg(" :( \ nLocation Not Enabled. Please Enable the location service and try again!!")
      })
    )
  }

  componentWillMount() {
    return this.getLocationFromDevice(); //request current gps location from device
  }

	render() {

    // create a location array to show the user location at the center of the map.
    const {latitude, longitude} = this.state.locationCords;
    var coords = [latitude, longitude];

    if(this.state.locationCords=[]){
      var coords = [130.8694928, -12.3713568]
    }

    console.log("Value of coords: "+ coords)
    //create a Stylsheet option to use over Map
    const style = MapboxGL.StyleSheet.create({
        icon: {
            iconImage: 'rsz_pin',
        }
    });

    // Main data of parking and restrooms. Should be moved to another file later.
    const data = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          id: '9d10456e-bdda-4aa9-9269-04c1667d4552',
          properties: {
            icon: 'example',
          },
          geometry: {
            type: 'Point',
            coordinates: [130.86758831345819, -12.37283454950172] ,
          },
        },
        {
          "type": "Feature",
          "properties": {
            "@id": "way/468230887",
            "amenity": "parking"
          },
          "geometry": {
            "type": "Polygon",
            "coordinates": [
              [
                [
                  130.8645678,
                  -12.3662746
                ],
                [
                  130.8642969,
                  -12.3666309
                ],
                [
                  130.8643973,
                  -12.3667065
                ],
                [
                  130.8646776,
                  -12.3663607
                ],
                [
                  130.8645678,
                  -12.3662746
                ]
              ]
            ]
          },
          "id": "way/468230887"
        },
        {
          "type": "Feature",
          "properties": {
            "@id": "way/544753282",
            "amenity": "parking"
          },
          "geometry": {
            "type": "Polygon",
            "coordinates": [
              [
                [
                  130.8719525,
                  -12.3710232
                ],
                [
                  130.8719605,
                  -12.3710756
                ],
                [
                  130.8719444,
                  -12.3711359
                ],
                [
                  130.872387,
                  -12.3712564
                ],
                [
                  130.8724326,
                  -12.3710992
                ],
                [
                  130.871974,
                  -12.3709472
                ],
                [
                  130.8719525,
                  -12.3710232
                ]
              ]
            ]
          },
          "id": "way/544753282"
        },
      ],
    };


    if (coords==[]) {
      return (
        <View>
          <Text> Location Updating... </Text>
        </View>
      )
    }
		return (
			<View style={styles.container}>
				<MapboxGL.MapView
					styleURL="mapbox://styles/uvishere/cjfvbwas63bm42sqcdevfph7j"
					zoomLevel={15}
					showUserLocation={true}
          onUserLocationUpdate={this.onUserLocationUpdate}
          userTrackingMode={MapboxGL.UserTrackingModes.Follow}
          centerCoordinate={coords}
					style={styles.container} >
        <MapboxGL.ShapeSource id="test" shape={data} images={{ assets: ['rsz_pin'] }}>
          <MapboxGL.SymbolLayer id="test" style={style.icon} textField={"Parking Location"}/>
            <MapboxGL.Callout title={"parking Location"} />
        </MapboxGL.ShapeSource>
        </MapboxGL.MapView>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
	},
  annotationContainer: {
    width: 30,
    height: 30,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
    borderRadius: 15,
  },
  annotationFill: {
    width: 30,
    height: 30,
    borderRadius: 15,
    backgroundColor: 'orange',
    transform: [{ scale: 0.6 }],
  },
});

