import React, { Component } from 'react';
import { StyleSheet, View, ImageBackground, TouchableOpacity  } from 'react-native';
import { Container, Header, Content, Text } from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
import {Actions, ActionConst} from 'react-native-router-flux'
import { Button, Icon } from 'react-native-elements'

let imagePath = require('../assets/images/finditbg.png') //image path for main background image. 

class Home extends Component {
	componentDidMount() {
    return console.log("Home Mounted!");
  };
  render() {
    const goToParking = () => {
      return Actions.directionsParking({
        theme:"parkingTheme"
      })
    };

    const goToRestroom = () => {
      return Actions.directions({
        theme:"toiletTheme"
      })
    };
    
    return (
      <ImageBackground source={imagePath}  imageStyle={{resizeMode: 'cover'}}  blurRadius={1} style={{flex:1}} >
        <Container>
          <Grid>
            <Row size={3} style={styles.quesTextGrid} >
              <Text style={styles.quesText}> What are you looking For? </Text>
              <Icon
                name='map-marker-radius'
                type='material-community'
                color='rgba(0,0,0,0.1)'
                size={140}
              />
            </Row>
            <Row  size={2} style={styles.mainGridWrapper} >
              <TouchableOpacity onPress={goToParking}>
                <Col style={styles.gridContentWrapper} >
                  <Icon
                    name='parking'
                    reverse
                    type='material-community'
                    color='rgba(0,0,0,0.3)'
                    size={50}
                  />
                  <Text style={styles.gridText} >Parking</Text>
                </Col>
              </TouchableOpacity>
              <TouchableOpacity onPress={goToRestroom}>
                <Col style={styles.gridContentWrapper} >
                  <Icon
                    name='human-male-female'
                    reverse
                    type='material-community'
                    color='rgba(0,0,0,0.3)'
                    size={50}
                  />
                  <Text style={styles.gridText} >Restroom</Text>
                </Col>
              </TouchableOpacity>
            </Row>
          </Grid>
        </Container>
    	</ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  mainGridWrapper: {
    justifyContent: 'space-evenly',
    flexDirection: 'row',
    alignItems: 'center',
    margin:10,
    backgroundColor: 'rgba(0,0,0,0.1)',
  },
  gridContentWrapper: {
    justifyContent: 'space-evenly',
    flexDirection: 'column',
    alignItems: 'center',

  },
  gridText:{
    textAlign: 'center',
    color:'#fff',
    marginTop: 20,
    fontSize: 20
  },
  quesTextGrid: {
    justifyContent: 'space-evenly',
    flexDirection: 'column',
    alignItems: 'center',
  },
  quesText: {
    color:'#fff',
    fontSize: 30

  }
});


export default Home;