'use strict';
import React, { Component } from 'react';
import { StyleSheet, ImageBackground } from 'react-native';
import { Container, Header, Content, H1, H3, Text } from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
import { Button, Icon } from 'react-native-elements'
import {Actions, ActionConst} from 'react-native-router-flux'

let imagePath = require('../assets/images/finditbg.png') //image path for main background image. 

class StartScreen extends Component {
	moveToHome() {
		return Actions.home();
	}
  render() {
  	const goToHome = () => { //Defining Router action here, to make the render function pure.
   		console.log("Go to Home Screen")
    	Actions.home()
    }
    return (
      <Container>
        <ImageBackground source={imagePath}  imageStyle={{resizeMode: 'cover'}}  blurRadius={1} style={{flex:1}} >
	    			<Grid>
	    				<Row size={4} style={styles.gridWrapper} >
	    					<H1 style={styles.welcomeText} >FindIt</H1>
	    					<Icon
	    					  name='wheelchair'
	    					  type='font-awesome'
	    					  color='#fff'
	    					  size={100}
	    					/>
	    					<Text style={styles.tagLine} > Disability friendly Facility Finder </Text>
	    				</Row>
	    				<Row size={1} style={styles.gridWrapper} >
	    					<Button
	    					  small
	    					  backgroundColor='rgba(0,0,0,0.1)'
	    					  iconRight={{name: 'location-arrow', type: 'font-awesome'}}
	    					  title='Get Started'
	    					  onPress={goToHome}
	    					  fontSize={30}
	    					  />
	    				</Row>	
	    			</Grid>
	    	</ImageBackground>	
      </Container>
    );
  }
}

const styles = StyleSheet.create({
	imageWrapper: {
		flex:1
	},
	gridWrapper:{
		flexDirection: 'column',
    justifyContent: 'space-evenly',
	},
	welcomeText: {
		textAlign: 'center',
		color: '#fff',
		fontSize:33,
		fontWeight: 'bold',
	},
	tagLine: {
		textAlign: 'center',
		color: 'rgba(255,255,255,0.5)',
		marginTop: 30
	}
})
export default StartScreen;
