# Find It - Disability Friendly Facility Finder

The major goal of this project is to increase the ease for disabled persons while searching the facilities available around their surroundings. Our project aims to decrease hassle while searching the parking lot space and toilets inside the CDU premises for the disabled.

### Version: 1.0

### Features
- Using mobile app, user can find out all the facilities available specially for disabled persons inside the CDU.
- Based on the location, user can find out the nearest facility available.
- Based on the location, user can be able to get the shortest route to the nearest facility required.

### Tech

Find It uses a number of projects including open source plugins to work properly:
* [node.js] - evented I/O for the backend
* [react-native] - Hybrid mobile application development framework by facebook.
* [mapbox] - Integrated Mapping Solution Based on Open Street Map.
* [turf.js] - Solution for mapping calculations.

### Installation
##### Requirements
* Node.js v6+
* React v16.0.0+
* React-native v0.52+
* mapbox v1.0.0+
* react-native-mapbox-gl v6.0+

Other Dependecies are listed in package.json file.

Clone the project from the project github or gitlab repo. Then, install the **Dependencies**, **devDependencies** and start the server.

```sh
$ git clone https://gitlab.com/uvishere/finditapp.git
$ cd finditapp 
$ npm install
$ react-native run android
```

For production environments...

```sh
$ npm install --production
$ react-native run android --variant-release
```

### License

The product will be released under GNU General Public License(GPL). The software is open source application, anyone can use our code and modify it for their purpose. Anyone can copy, modify, share and reuse the code in compliance to the GNU GPL license. The developer will not be responsible for any harm caused by the intentional or unintentional use of this software.
