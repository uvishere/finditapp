import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';
import Home from './src/components/Home';
import Routers from './src/Routers'
import StartScreen from './src/components/StartScreen';

//Application's main Entrance. Loads all the Routers first
export default class App extends Component {
	render() {
		return (
			<View style={styles.container}>
				<Routers /> 
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
	},
});
